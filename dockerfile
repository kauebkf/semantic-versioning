FROM python:3.8.2-slim-buster

RUN mkdir /app
WORKDIR /app
COPY requirements .
RUN pip install -r requirements

COPY ./src /src