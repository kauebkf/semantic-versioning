import sys
import requests

if __name__ == '__main__':
    mr_id = sys.argv[1].strip()

    get_response = requests.get('https://collect2.com/api/9d952097-605c-4a35-9a2b-12cc334d1b1d/datarecord/')
    data = get_response.json()['results']
    mr_id_list = [entry['record']['mr_id'] for entry in data]
    if mr_id not in mr_id_list:
        payload = {
            'mr_id': mr_id
        }

        create_response = requests.post(
            'https://collect2.com/api/9d952097-605c-4a35-9a2b-12cc334d1b1d/datarecord/',
            data=payload
        )
        if create_response.status_code != 200:
            raise SystemExit(f"Failed with error: {create_response.json()}")
    print('Merge Request information already saved.')

