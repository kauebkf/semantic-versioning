import requests
import sys

from utils import (
    serialize_tag,
    next_tag,
    get_mr_details,
    process_mr_data,
    get_latest_tag,
    create_tag,
    get_mr_id
)


if __name__ == '__main__':
    project_id = sys.argv[1].strip()
    access_token = sys.argv[2].strip()
    headers = {'PRIVATE-TOKEN': access_token}

    mr_id, api_item_id = get_mr_id()
    if mr_id is not None:
        mr_description, mr_ref, mr_state = get_mr_details(project_id, mr_id, headers)
        if mr_state == 'merged':
            tag_items_data = mr_description.split('#')[2:]
            for tag_item_data in tag_items_data:
                prefix, bump_type, is_release, release_description = process_mr_data(tag_item_data)
                latest_tag = get_latest_tag(project_id, prefix, headers)
                current_tag = serialize_tag(latest_tag)
                new_tag = next_tag(current_tag, bump_type)
                create_tag(project_id, new_tag, mr_ref, headers, is_release, release_description)
                response = requests.delete(f'https://collect2.com/api/9d952097-605c-4a35-9a2b-12cc334d1b1d/datarecord/{api_item_id}/')
        elif mr_state == 'closed':
            print('MR was closed, so I will not create any tags')
            response = requests.delete(f'https://collect2.com/api/9d952097-605c-4a35-9a2b-12cc334d1b1d/datarecord/{api_item_id}/')
        else:
            print('No merge requests are waiting for tagging')
