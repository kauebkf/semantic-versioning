import copy
import re
import requests
import os


def serialize_tag(version: str) -> list:
    match = re.match('([\\w\\W]+[-v]?)([0-9]+).([0-9]+).([0-9]+)', version)
    if not match:
        return None
    prefix = match.group(1)
    major = int(match.group(2))
    minor = int(match.group(3))
    patch = int(match.group(4))
    return [prefix, major, minor, patch]


def next_tag(tag: list, bump_type: str) -> str:
    new_tag = copy.deepcopy(tag)
    if bump_type == 'major':
        new_tag[1] += 1
        new_tag[2] = 0
        new_tag[3] = 0
    elif bump_type == 'minor':
        new_tag[2] += 1
        new_tag[3] = 0
    elif bump_type == 'patch':
        new_tag[3] += 1

    new_tag_string = f'{new_tag[0]}{new_tag[1]}.{new_tag[2]}.{new_tag[3]}'
    return new_tag_string


def get_mr_details(project_id, mr_id, headers):
    mr_details = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_id}', headers=headers)
    data = mr_details.json()
    mr_description = data['description']
    mr_ref = data['pipeline']['ref']
    mr_state = data['state']

    return mr_description, mr_ref, mr_state


def process_mr_data(tag_item_data):
    lines = tag_item_data.split('\n')
    prefix_line = lines[1]
    prefix = prefix_line[7:]
    is_release = 'x' in prefix_line
    major_line = lines[2]
    minor_line = lines[3]
    release_description = lines[5]
    bump_type = 'major' if 'x' in major_line else 'minor' if 'x' in minor_line else 'patch'

    return prefix, bump_type, is_release, release_description


def get_latest_tag(project_id, prefix, headers):
    get_tags_response = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}/repository/tags/', headers=headers)
    tags_data = get_tags_response.json()
    related_tag_names = [tag_data['name'] for tag_data in tags_data if prefix in tag_data['name']]
    latest_tag = related_tag_names[0] if len(related_tag_names) > 0 else f'{prefix}-v0.0.0'

    return latest_tag


def create_tag(project_id, new_tag, mr_ref, headers, is_release, release_description):
    data = {
        'id': project_id,
        'tag_name': new_tag,
        'ref': mr_ref
    }

    create_tags_response = requests.post(
        f'https://gitlab.com/api/v4/projects/{project_id}/repository/tags/',
        data=data,
        headers=headers
    )
    if create_tags_response.status_code == 201:
        if is_release:
            create_release_response = requests.post(
                f'https://gitlab.com/api/v4/projects/{project_id}/repository/tags/{new_tag}/release',
                data={'id': project_id, 'tag_name': new_tag, 'description': release_description},
                headers=headers
            )
            if create_release_response.status_code != 201:
                raise SystemExit(f'Failed with error: {create_release_response.json()}')
        return 'Operation completed successfully.'
    raise SystemExit(f'Failed with error: {create_tags_response.json()}')


def get_mr_id():
    response = requests.get('https://collect2.com/api/9d952097-605c-4a35-9a2b-12cc334d1b1d/datarecord/')
    data = response.json()['results'][0] if len(response.json()['results']) > 0 else None
    return data, None if data is None else data['record']['mr_id'], data['id']

