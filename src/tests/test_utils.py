from utils import serialize_tag, next_tag


def test_serializer_tag_success():
    tag = 'fa-backoffice-frontend-trainings-v1.0.52'
    serialized_tag = serialize_tag(tag)
    assert serialized_tag[0] == 'fa-backoffice-frontend-trainings-v'
    assert serialized_tag[1] == 1
    assert serialized_tag[2] == 0
    assert serialized_tag[3] == 52


def test_serializer_tag_success_without_v():
    tag = 'fa-backoffice-frontend-trainings-1.0.52'
    serialized_tag = serialize_tag(tag)
    assert serialized_tag[0] == 'fa-backoffice-frontend-trainings-'
    assert serialized_tag[1] == 1
    assert serialized_tag[2] == 0
    assert serialized_tag[3] == 52


def test_serializer_tag_success_without_dash_v():
    tag = 'fa-backoffice-frontend-trainings1.0.52'
    serialized_tag = serialize_tag(tag)
    assert serialized_tag[0] == 'fa-backoffice-frontend-trainings'
    assert serialized_tag[1] == 1
    assert serialized_tag[2] == 0
    assert serialized_tag[3] == 52


def test_serialize_tag_return_none_if_failed():
    tag = 'fa-.0.52'
    serialized_tag = serialize_tag(tag)
    assert serialized_tag is None


def test_bump_major():
    tag = ['test', 1, 2, 3]
    new_tag = next_tag(tag, 'major')
    assert new_tag == 'test2.0.0'


def test_bump_minor():
    tag = ['test', 1, 2, 3]
    new_tag = next_tag(tag, 'minor')
    assert new_tag == 'test1.3.0'


def test_bump_patch():
    tag = ['test', 1, 2, 3]
    new_tag = next_tag(tag, 'patch')
    assert new_tag == 'test1.2.4'
